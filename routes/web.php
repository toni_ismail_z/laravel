<?php

use App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::get('/', [HomeController::class, 'index']);

Route::get('/register', [AuthController::class, 'index']);

Route::post('/proses', [AuthController::class, 'store']);

Route::get('/admin', [Admin::class, 'index']);

Route::get('/table', [Admin::class, 'tables']);

Route::get('/data-tables', [Admin::class, 'data_tables']);
